﻿using System;
namespace LinqBasico
{
    public class Customer
    {
        public string NumeroCedula { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }

        public Customer(string numeroCedula, string apellidos, string nombres)
        {
            this.NumeroCedula = numeroCedula;
            this.Apellidos = apellidos;
            this.Nombres = nombres;
        }
    }
}
