﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqBasico
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            /*
            //First
            int[] scores = new int[] { 97, 92, 81, 60 };

            IEnumerable<int> ScoreQuery =
                from score in scores
                where score > 80
                select score;

            var numeroItems = ScoreQuery.Count();

            Console.WriteLine("Numero de items de la consulta: " + numeroItems);

            var maxItem = ScoreQuery.Max();
            var promedio = ScoreQuery.Average();
            var primero = ScoreQuery.First();

            foreach (var item in ScoreQuery)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();



            //Second
            string[] customers = new string[5] { "pepe", "camila", "lucho", "jorge", "camila" };
            var query =
                from customer in customers
                where customer == "camila"
                select customer;

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();

            */

            //Second part
            List<Customer> Customers = new List<Customer>();
            Customers.Add(new Customer("1", "Mera", "Andres"));
            Customers.Add(new Customer("2", "Lorente", "Ericka"));
            Customers.Add(new Customer("3", "Panchana", "Edgardo"));
            Customers.Add(new Customer("4", "Mera", "Luis"));

            var resultado = from cliente in Customers
                            where cliente.NumeroCedula == "2"
                            select cliente;

            foreach (var item in resultado)
            {
                Console.WriteLine(item.Apellidos + " " + item.Nombres);
            }

            Console.ReadKey();

            //Third
            LinqBasico.Factura.Factura factura = new LinqBasico.Factura.Factura();
            foreach (var item in factura.ListaProductos)
            {
                Console.WriteLine(item.ProductoItem.Marca + " " + item.ProductoItem.Modelo);
                Console.WriteLine(item.ProductoItem.Precio * item.Cantidad);
            }
        }
    }
}
