﻿using System;
namespace LinqBasico.Factura
{
    public class DetalleFactura
    {
        //Instrumento musical
        //Cantidad

        public Producto ProductoItem { get; set; }
        public int Cantidad { get; set; }


        public DetalleFactura(Producto producto, int cantidad)
        {
            this.ProductoItem = producto;
            this.Cantidad = cantidad;
        }
    }
}
