﻿using System;
using System.Collections.Generic;

namespace LinqBasico.Factura
{
    public class Factura
    {
        public CabeceraFactura Cabecera { get; set; }
        public List<DetalleFactura> ListaProductos { get; set; }

        public Factura()
        {
            Cabecera = new CabeceraFactura("1311311318", "Panchana", "Edgardo", "Manta");
            Producto Cocina = new Producto("Oster", "2020", 100.20M);
            Producto Refrigerador = new Producto("Mabe", "Antartica", 800.10M);

            ListaProductos = new List<DetalleFactura>();
            ListaProductos.Add(new DetalleFactura(Cocina, 2));
            ListaProductos.Add(new DetalleFactura(Refrigerador, 1));

        }
    }
}
