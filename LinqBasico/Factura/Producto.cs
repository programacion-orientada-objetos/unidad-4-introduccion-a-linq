﻿using System;
namespace LinqBasico.Factura
{
    public class Producto
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public decimal Precio { get; set; }

        public Producto(string marca, string modelo, decimal precio)
        {
            this.Marca = marca;
            this.Modelo = modelo;
            this.Precio = precio;
        }
    }
}
